package com.ash.port.outbound;

import java.util.Collection;

import com.ash.core.domain.User;

public interface UserRepository {
	String addNewUser(User user);

	User getUser(String id);

	boolean removeUser(String id);

	Collection<User> getAllUser();
}
